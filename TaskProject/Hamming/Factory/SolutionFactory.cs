using System;
using TaskProject.Hamming.Enum;
using TaskProject.Hamming.Interface;
using TaskProject.Hamming.Resource;

namespace TaskProject.Hamming.Factory
{
    /// <summary>
    /// 解決方案工廠
    /// </summary>
    public class SolutionFactory
    {
        /// <summary>
        /// 取得解決方案實體
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static IHammingSolution GetInstance(SolutionType type)
        {
            var t = SolutionResource.GetInstanceType(type);
            return (IHammingSolution)Activator.CreateInstance(t);
        }
    }
}