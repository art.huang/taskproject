﻿using System;
using ExpectedObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TaskProjectTests.PlusOne
{
    [TestClass]
    public class PlusOneTest
    {
        [TestMethod]
        public void Array_123_Should_Return_Array_124()
        {
            var sut = new TaskProject.PlusOne.PlusOne();
            var source = new int[] { 1, 2, 3 };
            var expected = new int[] { 1, 2, 4 };
            var actual = sut.Go(source);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod]
        public void Array_4321_Should_Return_Array_4322()
        {
            var sut = new TaskProject.PlusOne.PlusOne();
            var source = new int[] { 4, 3, 2, 1 };
            var expected = new int[] { 4, 3, 2, 2 };
            var actual = sut.Go(source);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod]
        public void Array_999_Should_Return_Array_2000()
        {
            var sut = new TaskProject.PlusOne.PlusOne();
            var source = new int[] { 9, 9, 9 };
            var expected = new int[] { 1, 0, 0, 0 };
            var actual = sut.Go(source);
            expected.ToExpectedObject().ShouldEqual(actual);
        }
    }
}