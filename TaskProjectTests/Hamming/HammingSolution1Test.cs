﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.Hamming.Enum;

namespace TaskProjectTests.Hamming
{
    [TestClass]
    public class HammingSolution1Test : BaseHammingSolutionTest
    {
        public HammingSolution1Test()
        {
            SolutionType = SolutionType.First;
        }
    }
}