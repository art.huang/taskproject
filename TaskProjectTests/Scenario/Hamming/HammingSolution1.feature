﻿
#language: zh-TW

功能: HammingCompute
場景大綱:Hamming
	假設 我採用解決方案 <Solution>
	而且 我輸入 <InputNumber>
    當 我呼叫 GetAnser 方法
    那麼 結果應為 <Result>
    例子:
    | Solution | InputNumber | Result |
    | 1        | 11		     | 3	  |
    | 1        | 33		     | 2	  |
    | 1        | 128	     | 1	  |
    | 2        | 11			 | 3	  |
    | 2        | 33			 | 2	  |
    | 2        | 128	     | 1	  |
    | 3        | 11			 | 3	  |
    | 3        | 33			 | 2	  |
    | 3        | 128	     | 1	  |

