# LeetCode 練習

## Jenkins

| Job Name      | URL                                        | Describe                            |
| ------------- | ------------------------------------------ | ----------------------------------- |
| TaskProjectCI | http://jenkins.art:8081/job/TaskProjectCI/ | Pipeline [Jenkinsfile][jenkinsfile] |

[jenkinsfile]: ./Jenkinsfile
