namespace TaskProject.Hamming.Enum
{
    /// <summary>
    /// 解決方案列舉
    /// </summary>
    public enum SolutionType
    {
        /// <summary>
        /// 第一種解決方案
        /// </summary>
        First,

        /// <summary>
        /// 第二種解決方案
        /// </summary>
        Second,

        /// <summary>
        /// 第三種解決方案
        /// </summary>
        Third,

        /// <summary>
        /// 未實作的解決方案
        /// </summary>
        NotExist
    }
}