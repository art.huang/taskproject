﻿using TaskProject.Hamming.Interface;

namespace TaskProject.Hamming.Service
{
    public class SolutionThree : IHammingSolution
    {
        public int GetAnswer(uint target)
        {
            // 8 => 1000
            // 8/2 = 4  mod = 0
            // 4/2 = 2  mod = 0
            // 2/2 = 1  mod = 0

            // 11 => 1011
            // 11/2 = 5  mod = 1
            // 5/2 = 2  mod = 1
            // 2/2 = 1  mod = 0
            uint result = 0;
            while (target > 1)
            {
                var originTarget = target;
                target = target / 2;
                result = (target == 1)
                    ? result + 1
                    : result + originTarget % 2;
            }
            return (int)result;
        }
    }
}