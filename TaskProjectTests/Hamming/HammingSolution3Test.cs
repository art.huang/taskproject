﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.Hamming.Enum;

namespace TaskProjectTests.Hamming
{
    [TestClass]
    public class HammingSolution3Test : BaseHammingSolutionTest
    {
        public HammingSolution3Test()
        {
            SolutionType = SolutionType.Third;
        }
    }
}