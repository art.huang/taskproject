﻿using System.Linq;

namespace TaskProject.PlusOne
{
    public class PlusOne
    {
        public int[] Go(int[] source)
        {
            var carry = 1;
            var index = source.Length - 1;
            while (carry != 0 && index > -1)
            {
                var newVal = carry + source[index];
                carry = newVal / 10;
                source[index] = newVal % 10;
                index--;
            }
            return carry == 1 ? new[] { 1 }.Concat(source).ToArray() : source;
        }
    }
}