﻿using System;

namespace TaskProject.ExcelSheetColumnNumber
{
    public class ExcelSheetColumnNumberTask
    {
        private const int MaxEnglishLength = 26;
        private const int AsciiBase = 64;

        /// <summary>
        /// Given a column title as appear in an Excel sheet, return its corresponding column number.
        ///  For example:
        ///     A -> 1   ,   B -> 2 ,    C -> 3
        ///     Z -> 26 AA -> 27  AB -> 28
        ///     Case 1 Input "A" Output 1
        ///     Case 2 Input "AB Output 28
        ///     Case 3 Input "ZY" Output 701
        /// </summary>
        /// <param name="source">The s.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public int TitleToNumber(string source)
        {
            var result = 0;
            var power = source.Length - 1;
            foreach (var c in source)
            {
                var idxNum = GetEnglishCharNumber(c);
                result += CountWithLevel(idxNum, power);
                power--;
            }
            return result;
        }

        /// <summary>
        /// 計算該階層英文字母所代表的數字
        /// </summary>
        /// <param name="idxNum">英文字母的Index.</param>
        /// <param name="power">第幾個階層.</param>
        /// <returns>回傳該階層英文字母所代表的數字</returns>
        private int CountWithLevel(int idxNum, int power)
        {
            return power == 0
                ? idxNum
                : idxNum * (int)Math.Pow(MaxEnglishLength, power);
        }

        /// <summary>
        /// 取得英文字母的 Index Number (ASCII碼減去64)
        /// </summary>
        /// <param name="letter">英文字母</param>
        /// <returns></returns>
        private int GetEnglishCharNumber(char letter) => letter - AsciiBase;
    }
}