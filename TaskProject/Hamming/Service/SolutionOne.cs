﻿using System;
using TaskProject.Hamming.Interface;

namespace TaskProject.Hamming.Service
{
    public class SolutionOne : IHammingSolution
    {
        public int GetAnswer(uint target)
        {
            // TODO:嘗試自行撰寫數字轉二進位字串
            //var binaryStr = Convert.ToString(n, 2);
            //return binaryStr.Count(c => c == '1');

            var result = 0;
            for (var i = 10; 0 <= i; i--)
            {
                var val = (uint)Math.Pow(2, i);
                if (target < val) continue;

                target -= val;
                result++;
            }
            return result;
        }
    }
}