using System;
using TaskProject.Hamming.Interface;

namespace TaskProject.Hamming.Service
{
    public class SolutionTwo : IHammingSolution
    {
        public int GetAnswer(uint target)
        {
            var result = 0;
            for (var i = 10; 0 <= i; i--)
            {
                var val = (uint)Math.Pow(2, i);
                if (target < val) continue;

                target -= val;
                result++;
            }
            return result;
        }
    }
}