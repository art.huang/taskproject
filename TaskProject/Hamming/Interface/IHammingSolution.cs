namespace TaskProject.Hamming.Interface
{
    public interface IHammingSolution
    {
        /// <summary>
        /// 取得解答
        /// </summary>
        /// <param name="target">正整數</param>
        /// <returns></returns>
        int GetAnswer(uint target);
    }
}