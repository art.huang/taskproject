﻿using ExpectedObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.FIzzBuzz;

namespace TaskProjectTests.FizzBuzz
{
    [TestClass]
    public class FizzBuzzTaskTests
    {
        //編寫一個程序，輸出從1到n的數字的字符串表示。
        //但對於三倍的倍數，它應輸出“Fizz”而不是數字，
        //並輸出五個輸出“Buzz”的倍數。對於三個和五個輸出“FizzBu​​zz”的倍數的數字。
        // example: n = 15,
        //Return:
        //[
        //    "1",
        //    "2",
        //    "Fizz",
        //    "4",
        //    "Buzz",
        //    "Fizz",
        //    "7",
        //    "8",
        //    "Fizz",
        //    "Buzz",
        //    "11",
        //    "Fizz",
        //    "13",
        //    "14",
        //    "FizzBuzz"
        //]
        [TestMethod]
        public void Test_N_Equal_15()
        {
            var sut = new FizzBuzzTask();
            var expected = new[]
            {
                "1",
                "2",
                "Fizz",
                "4",
                "Buzz",
                "Fizz",
                "7",
                "8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "14",
                "FizzBuzz"
            };
            var actual = sut.FizzBuzz(15);
            expected.ToExpectedObject().ShouldEqual(actual);
        }
    }
}