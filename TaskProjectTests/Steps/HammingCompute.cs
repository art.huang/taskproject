﻿using FluentAssertions;
using TaskProject.Hamming.Enum;
using TaskProject.Hamming.Factory;
using TaskProject.Hamming.Interface;
using TechTalk.SpecFlow;

namespace TaskProjectTests.Steps
{
    [Binding]
    public class HammingCompute
    {
        protected SolutionType SolutionType;

        protected IHammingSolution GetInstance()
        {
            return SolutionFactory.GetInstance(SolutionType);
        }

        [Given(@"我採用解決方案 (.*)")]
        public void 假設我採用解決方案(int sol)
        {
            switch (sol)
            {
                case 1:
                    SolutionType = SolutionType.First;
                    break;

                case 2:
                    SolutionType = SolutionType.Second;
                    break;

                case 3:
                    SolutionType = SolutionType.Third;
                    break;
            }
        }

        [Given(@"我輸入 (.*)")]
        public void 假設我輸入(uint num)
        {
            ScenarioContext.Current.Set(num, "num");
        }

        [When(@"我呼叫 GetAnser 方法")]
        public void 當我呼叫GetAnser方法()
        {
            var num = ScenarioContext.Current.Get<uint>("num");
            var sut = GetInstance();
            var actual = sut.GetAnswer(num);
            ScenarioContext.Current.Set(actual, "actual");
        }

        [Then(@"結果應為 (.*)")]
        public void 那麼結果應為(int expected)
        {
            var actual = ScenarioContext.Current.Get<int>("actual");
            actual.Should().Be(expected);
        }
    }
}