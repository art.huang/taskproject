﻿using System.Collections.Generic;
using ExpectedObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.ThreeSum;

namespace TaskProjectTests.ThreeSum
{
    [TestClass()]
    public class ThreeSumTaskTests
    {
        private ThreeSumTask _sut;

        [TestInitialize]
        public void BeforeEach()
        {
            _sut = new ThreeSumTask();
        }

        [TestCleanup]
        public void AfterEach()
        {
            _sut = null;
        }

        [TestMethod]
        public void test_empty()
        {
            //arrange
            var nums = new int[] { };
            var expected = new List<IList<int>>();

            //act
            var actual = _sut.ThreeSum(nums);

            //assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod]
        public void test_zero()
        {
            //arrange
            var nums = new int[] { 0, 0, 0 };
            var expected = new List<IList<int>>()
            {
                new List<int>(){ 0,0,0 },
            };

            // Act
            var actual = _sut.ThreeSum(nums);

            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod]
        public void test_1_0_m1()
        {
            // Arrange
            var nums = new int[] { 1, 0, -1 };
            var expected = new List<IList<int>>()
            {
                new List<int>(){ -1,-0, 1 }
            };

            // Act
            var actual = _sut.ThreeSum(nums);

            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod]
        public void test_m2_m1_0_3()
        {
            // Arrange
            var nums = new int[] { -2, -1, 0, 3 };
            var expected = new List<IList<int>>()
            {
                new List<int>(){ -2,-1, 3 }
            };

            // Act
            var actual = _sut.ThreeSum(nums);

            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod]
        public void test_m1_m2_0_3()
        {
            // Arrange
            var nums = new int[] { -1, -2, 0, 3, 1, -1 };
            var expected = new List<IList<int>>()
            {
                new List<int>(){ -2, -1, 3 },
                new List<int>(){ -1, 0 ,1 }
            };
            // Act
            var actual = _sut.ThreeSum(nums);

            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod()]
        public void test_default_case()
        {
            // Arrange
            var nums = new int[] { -1, 0, 1, 2, -1, -4 };
            var expected = new List<IList<int>>()
            {
                new List<int>(){ -1,-1,2},
                new List<int>(){-1,0,1 }
            };

            // Act
            var actual = _sut.ThreeSum(nums);

            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        [TestMethod()]
        public void 測試陣列右邊界數字重複()
        {
            // Arrange
            var nums = new int[] { -3, -2, 0, 1, 2, 2 };
            var expected = new List<IList<int>>()
            {
                new List<int>(){ -2,0,2},
                new List<int>(){ -3,1,2}
            };

            // Act
            var actual = _sut.ThreeSum(nums);

            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            expected.ToExpectedObject().ShouldEqual(actual);
        }
    }
}