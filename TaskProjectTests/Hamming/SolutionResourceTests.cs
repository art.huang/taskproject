﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.Hamming.Enum;
using TaskProject.Hamming.Factory;
using TaskProject.Hamming.Service;

namespace TaskProjectTests.Hamming
{
    [TestClass]
    public class SolutionResourceTests
    {
        [TestMethod]
        public void Solution_One_Instance_Check()
        {
            var type = SolutionType.First;
            var expectedType = typeof(SolutionOne);
            var actual = SolutionFactory.GetInstance(type);
            Assert.IsInstanceOfType(actual, expectedType);
        }

        [TestMethod]
        public void Solution_Two_Instance_Check()
        {
            var type = SolutionType.Second;
            var expectedType = typeof(SolutionTwo);
            var actual = SolutionFactory.GetInstance(type);
            Assert.IsInstanceOfType(actual, expectedType);
        }

        [TestMethod]
        public void Solution_Third_Instance_Check()
        {
            var type = SolutionType.Third;
            var expectedType = typeof(SolutionThree);
            var actual = SolutionFactory.GetInstance(type);
            Assert.IsInstanceOfType(actual, expectedType);
        }

        [TestMethod]
        public void Solution_NotExist_Should_Throw_Error()
        {
            var type = SolutionType.NotExist;
            Action actual = () => SolutionFactory.GetInstance(type);
            actual.Should().Throw<ArgumentException>().WithMessage("No Solution");
        }
    }
}