﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.Hamming.Enum;

namespace TaskProjectTests.Hamming
{
    [TestClass]
    public class HammingSolution2Test : BaseHammingSolutionTest
    {
        public HammingSolution2Test()
        {
            SolutionType = SolutionType.Second;
        }
    }
}