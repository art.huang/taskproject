using System;
using System.Collections.Generic;
using TaskProject.Hamming.Enum;
using TaskProject.Hamming.Service;

namespace TaskProject.Hamming.Resource
{
    /// <summary>
    /// 解決方案資源類別
    /// </summary>
    internal class SolutionResource
    {
        private static readonly Dictionary<SolutionType, Type> Resources;

        static SolutionResource()
        {
            // 將所有解決方案都列在這裡，存放在Dictionary中
            // 有新的解決方案就在這邊調整即可
            Resources = new Dictionary<SolutionType, Type>
            {
                [SolutionType.First] = typeof(SolutionOne),
                [SolutionType.Second] = typeof(SolutionTwo),
                [SolutionType.Third] = typeof(SolutionThree)
            };
        }

        public static Type GetInstanceType(SolutionType type)
        {
            // 透過Dictionary的方法判斷是否存在該解決方案
            if (Resources.ContainsKey(type))
            {
                return Resources[type];
            }

            throw new ArgumentException("No Solution");
        }
    }
}