﻿using System.Collections.Generic;

namespace TaskProject.FIzzBuzz
{
    public class FizzBuzzTask
    {
        public IList<string> FizzBuzz(int n)
        {
            var list = new string[n];
            for (var i = 1; i <= n; i++)
            {
                var temp = "";
                if (i % 3 == 0)
                {
                    temp += "Fizz";
                }
                if (i % 5 == 0)
                {
                    temp += "Buzz";
                }
                if (temp == "")
                {
                    temp = i.ToString();
                }
                list[i - 1] = temp;
            }
            return list;
        }
    }
}