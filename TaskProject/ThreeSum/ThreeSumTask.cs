﻿using System;
using System.Collections.Generic;

namespace TaskProject.ThreeSum
{
    public class ThreeSumTask
    {
        private const int Answer = 0;

        /// <summary>
        /// Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
        /// Note:
        ///     The solution set must not contain duplicate triplets.
        /// </summary>
        /// <example>
        /// Given array nums = [-1, 0, 1, 2, -1, -4],
        ///     A solution set is:
        ///     [
        ///         [-1, 0, 1],
        ///         [-1, -1, 2]
        ///     ]
        /// </example>
        /// <param name="nums">Array nums.</param>
        /// <returns></returns>
        public IList<IList<int>> ThreeSum(int[] nums)
        {
            Array.Sort(nums);
            return GetMatchAnswerSet(nums, Answer);
        }

        /// <summary>
        /// 取得符合答案的設定組
        /// </summary>
        /// <param name="nums">傳入的條件</param>
        /// <param name="answer">回傳的答案</param>
        /// <returns></returns>
        private List<IList<int>> GetMatchAnswerSet(int[] nums, int answer)
        {
            var result = new List<IList<int>>() { };

            for (int mainIdx = 0; mainIdx < nums.Length - 2; mainIdx++)
            {
                var leftIdx = mainIdx + 1;          //左邊的index
                var rightIdx = nums.Length - 1;     //右邊的index

                if (mainIdx > 0 && nums[mainIdx] == nums[mainIdx - 1]) continue;

                while (leftIdx < rightIdx)          // 左右index未交集才執行
                {
                    var total = GetTotal(nums, mainIdx, leftIdx, rightIdx); // 三個index的數字加起來要等於answer
                    if (total == answer)
                    {
                        // apply to result
                        var currectSet = new List<int>() { nums[mainIdx], nums[leftIdx], nums[rightIdx] };
                        result.Add(currectSet);

                        // 滿足條件的INDEX使用過後，要依序再指向下一個數字
                        leftIdx++;
                        rightIdx--;

                        // 左邊INDEX的數字與上一次相同，且左右INDEX尚未碰觸，則略過當前數字
                        while (nums[leftIdx] == nums[leftIdx - 1] && leftIdx < rightIdx)
                        {
                            leftIdx++;
                        }

                        // 右邊INDEX的數字若與上一次相同，且左右INDEX尚未碰觸，則略過當前數字
                        while (nums[rightIdx] == nums[rightIdx + 1] && leftIdx < rightIdx)
                        {
                            rightIdx--;
                        }
                    }
                    else if (total > answer)
                    {
                        // 三個數字加起來比較大，所以要把右邊INDEX往前一個
                        rightIdx--;
                    }
                    else
                    {
                        // 三個數字加起來比目標要少，將左邊的INDEX往後
                        leftIdx++;
                    }
                }
            }

            return result;
        }

        private static int GetTotal(int[] nums, int mainIdx, int leftIdx, int rightIdx)
        {
            return nums[mainIdx] + nums[leftIdx] + nums[rightIdx];
        }
    }
}