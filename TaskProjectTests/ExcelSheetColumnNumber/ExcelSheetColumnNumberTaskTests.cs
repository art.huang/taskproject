﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.ExcelSheetColumnNumber;

namespace TaskProjectTests.ExcelSheetColumnNumber
{
    [TestClass()]
    public class ExcelSheetColumnNumberTaskTests
    {
        private ExcelSheetColumnNumberTask _sut;

        [TestInitialize]
        public void BeforeEach()
        {
            _sut = new ExcelSheetColumnNumberTask();
        }

        ///     Case 1 Input "A" Output 1
        ///     Case 2 Input "AB Output 28
        ///     Case 3 Input "ZY" Output 701
        [TestMethod()]
        public void Input_A_Return_1()
        {
            var title = "A";
            var expected = 1;
            var actual = _sut.TitleToNumber(title);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Input_AB_Return_28()
        {
            var title = "AB";
            var expected = 28;
            var actual = _sut.TitleToNumber(title);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Input_ZY_Return_701()
        {
            var title = "ZY";
            var expected = 701;
            var actual = _sut.TitleToNumber(title);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Input_ABC_Return_731()
        {
            var title = "ABC";
            var expected = 731;
            var actual = _sut.TitleToNumber(title);
            Assert.AreEqual(expected, actual);
        }
    }
}