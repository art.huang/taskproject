using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskProject.Hamming.Enum;
using TaskProject.Hamming.Factory;
using TaskProject.Hamming.Interface;

namespace TaskProjectTests.Hamming
{
    public abstract class BaseHammingSolutionTest
    {
        protected SolutionType SolutionType;

        protected IHammingSolution GetInstance()
        {
            return SolutionFactory.GetInstance(SolutionType);
        }

        [TestMethod]
        public void Input_11_Should_Return_3()
        {
            uint target = 11;
            var expected = 3;
            var sut = GetInstance();
            var actual = sut.GetAnswer(target);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Input_128_Should_Return_1()
        {
            uint target = 128;
            var expected = 1;
            var sut = GetInstance();
            var actual = sut.GetAnswer(target);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Input_33_Should_Return_2()
        {
            uint target = 33;
            var expected = 2;
            var sut = GetInstance();
            var actual = sut.GetAnswer(target);
            Assert.AreEqual(expected, actual);
        }
    }
}